﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct StickInfo {

	public int Level;
	public float UnroundedLevel;
	public int Mass;
	public int Square;

	public StickInfo(int level, int mass, int square, float unroundedLevel)
	{
		Level = level;
		Mass = mass;
		Square = square;
		UnroundedLevel = unroundedLevel;
	}
}
