﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
	public class NewGameController : NetworkManager
	{
		#region

		public static NewGameController Instance { get; private set; }

		#endregion

		public List<Player> m_players = new List<Player>();
		private GameObject localPlayer;
		public GameObject[] Sticks;

		public GameObject m_timePanel;
		public GameObject m_networkButtonsPanel;
		public GameObject menuBackground;
		public Button m_exitButton;
		public GameObject m_gameResultsPanel;
		public Timer m_timer;
		public GameObject m_sphinxPanel;
		public GameObject m_waitingForSecondPlayerPanel;

		private StickInfo[] stickInfos = new[]
		{
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
			new StickInfo(),
		};

		public UnityEngine.UI.Text hostNameInput;

		private Vector2Int[] _stickPairs = new[]
		{
			new Vector2Int(1, 11),
			new Vector2Int(2, 4),
			new Vector2Int(3, 9),
			new Vector2Int(0, 5),
			new Vector2Int(6, 14),
			new Vector2Int(7, 13),
			new Vector2Int(8, 12),
			new Vector2Int(10, 15)
		};

		public bool IsPlaying = false;

		public bool IsWin = false;

		private bool _isServer = false;

		private decimal _pushCoef = 141m;
		private bool _wasWinResultShowed = false;

		public GameObject PlayingPlayer;

		public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
		{
			_isServer = true;
			m_waitingForSecondPlayerPanel.SetActive(true);
			int playersCount = NetworkServer.connections.Count;
			if (playersCount <= 2)
			{
				localPlayer = Instantiate(
					playerPrefab,
					playerPrefab.transform.position,
					Quaternion.identity);

				NetworkServer.AddPlayerForConnection(conn, localPlayer, playerControllerId);

				m_players.Add(localPlayer.GetComponent<Player>());

				if (playersCount == 2)
				{
					m_waitingForSecondPlayerPanel.SetActive(false);
					GenerateFirstLevels();
					GenerateFirstSquares();

					m_players[0].Id = 0;
					m_players[1].Id = 1;

					m_players[0].InitSticckInfos(stickInfos);
					m_players[1].InitSticckInfos(stickInfos);

					m_players[0].RpcInitSticks();
					m_players[1].CmdInitSticks();
					m_players[1].RpcSetTagPlayer2();

					IsPlaying = true;
					m_players[0].RpcSetPlayingTrue();
					m_players[0].RpcShowTime();
					m_players[1].CmdShowTime();

				}
			}
			else
			{
				conn.Disconnect();
			}
		}

		public override void OnStopHost()
		{
			Debug.Log("Host stop");
			NetworkServer.DisconnectAll();
		}

		public override void OnStopClient()
		{
			Debug.Log("Client stop");
			base.OnStopClient();
		}

		public override void OnClientDisconnect(NetworkConnection conn)
		{
			Debug.Log("OnClientDisconnect");
			base.OnClientDisconnect(conn);
			conn.Disconnect();
		}

		private void Awake()
		{
			Instance = this;
			m_timePanel.SetActive(false);
			m_sphinxPanel.SetActive(false);
			m_gameResultsPanel.SetActive(false);
			m_exitButton.gameObject.SetActive(false);
		}

		public void StartLocalGame()
		{
			StartHost();
			m_networkButtonsPanel.SetActive(false);
			m_gameResultsPanel.SetActive(false);
			menuBackground.SetActive(false);
			m_sphinxPanel.SetActive(true);
			m_timePanel.SetActive(true);
			m_exitButton.gameObject.SetActive(true);
		}

		public void JoinLocalGame()
		{
			if (hostNameInput.text != "Enter your IP...")
			{
				networkAddress = hostNameInput.text;
			}
			StartClient();
			m_networkButtonsPanel.SetActive(false);
			m_gameResultsPanel.SetActive(false);
			menuBackground.SetActive(false);
			m_sphinxPanel.SetActive(true);
			m_exitButton.gameObject.SetActive(true);
		}

		public void ExitGame()
		{
			if (NetworkServer.active)
			{
				StopServer();
			}
			if (NetworkClient.active)
			{
				StopClient();
			}
			m_players.Clear();
			m_networkButtonsPanel.SetActive(true);
			menuBackground.SetActive(true);
			m_sphinxPanel.SetActive(false);
			m_exitButton.gameObject.SetActive(false);
			Application.Quit();
		}

		void Start()
		{
			InitStickPairs();
			m_gameResultsPanel.SetActive(false);
		}

		void Update()
		{
			if (_isServer && IsPlaying)
			{
				CheckIsWin();
				if (IsWin && !_wasWinResultShowed)
				{
					/*var winTime = (float)Math.Round(m_timer.GetTime(), 2);
					ShowGameResult("Win!\n It took: " + winTime + " s");*/
					//m_players[0].CmdShowWinResult();
					//m_players[1].RpcShowWinResult();
					_wasWinResultShowed = true;
				}
			}
		}

		void InitStickPairs()
		{
			foreach (var stickPair in _stickPairs)
			{
				SetConnectedStick(stickPair.x, stickPair.y);
				SetBlueArrow(stickPair.y);
				Sticks[stickPair.x].GetComponent<Stick>().IsMainStick = true;
				Sticks[stickPair.x].GetComponent<Stick>().Id = stickPair.x;
				Sticks[stickPair.y].GetComponent<Stick>().Id = stickPair.y;
			}
		}

		void SetConnectedStick(int firstItem, int secondItem)
		{
			Sticks[firstItem].GetComponent<Stick>().ConnectedStick = Sticks[secondItem];
			Sticks[secondItem].GetComponent<Stick>().ConnectedStick = Sticks[firstItem];
		}

		void SetSquareOfContainer(int firstItem, int secondItem)
		{
			int firstSquare = Random.Range(1, 5);
			int secondSquare = GenerateSecondSquare(firstSquare);

			stickInfos[firstItem].Square = firstSquare;
			stickInfos[secondItem].Square = secondSquare;
		}

		private void SetBlueArrow(int itemNumber)
		{
			Sticks[itemNumber].GetComponent<Stick>().SetArrowBlueColor();
		}

		private int GenerateSecondSquare(int firstSquare)
		{
			int secondSquare = Random.Range(1, 5);
			while (secondSquare == firstSquare)
			{
				secondSquare = Random.Range(1, 5);
			}

			return secondSquare;
		}

		public void InterractWithTouchedStick(GameObject touchedStick)
		{
			int playerId = m_players.Count != 0 ? 0 : 1;

			if (m_players.Count == 0)
			{
				/*GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
				GameObject secondPlayer;
				if (players[0].GetComponent<Player>().Id == 1)
				{
					secondPlayer = players[0];
				}
				else
				{
					secondPlayer = players[1];
				}*/
				var secondPlayer = GameObject.FindWithTag("Player2");
				secondPlayer.GetComponent<Player>().ChangeNewControllerStickInfos(playerId, touchedStick.GetComponent<Stick>().Id);
			}
			else
			{
				localPlayer.GetComponent<Player>().ChangeNewControllerStickInfos(playerId, touchedStick.GetComponent<Stick>().Id);
			}
		}

		public void UpdateSticks(int playerId, int mainStickIndex, int playerMass)
		{
			CalculateHighForStickInfos(playerId, mainStickIndex, playerMass);
			m_players[0].SetSticckInfos(stickInfos);
			m_players[1].SetSticckInfos(stickInfos);
			m_players[0].RpcInitSticks();
			m_players[1].CmdInitSticks();
		}
	
		public void CalculateHighForStickInfos(int playerId, int mainStickIndex, int playerMass)
		{
			stickInfos[mainStickIndex].Mass += playerMass;
			GameObject connectedStick = Sticks[mainStickIndex].GetComponent<Stick>().ConnectedStick;
			int connectedStickIndex = connectedStick.GetComponent<Stick>().Id;

			bool isSwaped = false;
			if (!Sticks[mainStickIndex].GetComponent<Stick>().IsMainStick)
			{
				var temp = connectedStickIndex;
				connectedStickIndex = mainStickIndex;
				mainStickIndex = temp;
				isSwaped = true;
			}

			decimal dH = stickInfos[mainStickIndex].Mass / (decimal) stickInfos[mainStickIndex].Square -
			             stickInfos[connectedStickIndex].Mass / (decimal) stickInfos[connectedStickIndex].Square;

			CalculateUnroundedLevels(mainStickIndex, connectedStickIndex, dH, isSwaped);

			stickInfos[mainStickIndex].Level = Math.Min(20, Math.Max(0, (int) Math.Round(stickInfos[mainStickIndex].UnroundedLevel)));
			stickInfos[connectedStickIndex].Level = Math.Min(20, Math.Max(0, (int) Math.Round(stickInfos[connectedStickIndex].UnroundedLevel)));
		}

		public void CalculateSticks()
		{
			foreach (var stick in Sticks)
			{
				stick.GetComponent<Stick>().CalculateHigh();
			}
		}

		/*public void NewCalculateHigh(GameObject mainStick)
		{
			GameObject connectedStick = mainStick.GetComponent<Stick>().ConnectedStick;
			if (!mainStick.GetComponent<Stick>().IsMainStick)
			{
				var temp = connectedStick;
				connectedStick = mainStick;
				mainStick = temp;
			}

			decimal dH = mainStick.GetComponent<Stick>().Mass / (decimal) mainStick.GetComponent<Stick>().SquareOfContainer -
			             connectedStick.GetComponent<Stick>().Mass / (decimal) connectedStick.GetComponent<Stick>().SquareOfContainer;

			SetUnroundedLevels(mainStick, connectedStick, dH);

			mainStick.GetComponent<Stick>().Level = Math.Min(20, Math.Max(0, (int) Math.Round(mainStick.GetComponent<Stick>().UnRoundedLevel)));
			connectedStick.GetComponent<Stick>().Level =
				Math.Min(20, Math.Max(0, (int) Math.Round(connectedStick.GetComponent<Stick>().UnRoundedLevel)));

			mainStick.GetComponent<Stick>().CalculateHigh();
			connectedStick.GetComponent<Stick>().CalculateHigh();
		}*/

		private void SetUnroundedLevels(GameObject mainStick, GameObject connectedStick, decimal dH)
		{
			mainStick.GetComponent<Stick>().UnRoundedLevel = (float) (-1 * (dH * connectedStick.GetComponent<Stick>().SquareOfContainer /
			                                                                (mainStick.GetComponent<Stick>().SquareOfContainer +
			                                                                 (decimal) connectedStick.GetComponent<Stick>().SquareOfContainer)));
			connectedStick.GetComponent<Stick>().UnRoundedLevel = (20f - mainStick.GetComponent<Stick>().UnRoundedLevel);

		}

		private void CalculateUnroundedLevels(int mainStickIndex, int connectedStickIndex, decimal dH, bool isSwaped)
		{
			float tempUnroundedLevel = (float) (-1 * (dH * stickInfos[connectedStickIndex].Square /
			                                          (stickInfos[mainStickIndex].Square +
			                                           (decimal) stickInfos[connectedStickIndex].Square)));
			if (isSwaped && stickInfos[connectedStickIndex].UnroundedLevel < (20f - tempUnroundedLevel))
			{
				stickInfos[connectedStickIndex].UnroundedLevel = tempUnroundedLevel;
				stickInfos[mainStickIndex].UnroundedLevel = (20f - stickInfos[connectedStickIndex].UnroundedLevel);
			}
			else
			{
				stickInfos[mainStickIndex].UnroundedLevel = tempUnroundedLevel;
				stickInfos[connectedStickIndex].UnroundedLevel = (20f - stickInfos[mainStickIndex].UnroundedLevel);
			}
		}

		private void GenerateFirstLevels()
		{
			foreach (var stickPair in _stickPairs)
			{
				stickInfos[stickPair.x].Level = Random.Range(0, 20);
				stickInfos[stickPair.y].Level = (20 - stickInfos[stickPair.x].Level);
			}
		}

		private void GenerateFirstSquares()
		{
			foreach (var stickPair in _stickPairs)
			{
				SetSquareOfContainer(stickPair.x, stickPair.y);
			}
		}

		public void SetGeneratedSticksInfo(StickInfo[] getStickInfos)
		{
			foreach (var stick in Sticks)
			{
				int stickId = stick.GetComponent<Stick>().Id;
				stick.GetComponent<Stick>().Set(getStickInfos[stickId].Mass, getStickInfos[stickId].Square, getStickInfos[stickId].Level,
					getStickInfos[stickId].UnroundedLevel);
				stick.GetComponent<Stick>().CalculateHigh();
			}
		}

		private void CheckIsWin()
		{
			foreach (var pair in _stickPairs)
			{
				if (!Sticks[pair.x].GetComponent<Stick>().IsBalanced)
				{
					return;
				}
			}

			IsWin = true;
			m_players[0].RpcSetIsWinTrue();
		}

		public void ShowGameResult(string winText)
		{
			m_gameResultsPanel.SetActive(true);
			m_gameResultsPanel.GetComponentInChildren<Text>().text = winText;
		}

		private void SetStickBalanced()
		{
			foreach (var stick in Sticks)
			{
				stick.GetComponent<Stick>().IsBalanced = true;
			}
		}

		public void SetWeight(int weight)
		{
			if(m_players.Count == 0)
			{
				GameObject.FindWithTag("Player2").GetComponent<Player>().SetMass(weight);
			}
			else
			{
				m_players[0].SetMass(weight);
			}
		}

		public void SetWeight1()
		{
			if (m_players.Count == 0)
			{
				GameObject.FindWithTag("Player2").GetComponent<Player>().SetMass(1);
			}
			else
			{
				m_players[0].SetMass(1);
			}
		}
	}
}
