﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using UnityEngine;
using UnityEngine.EventSystems;

public class StickTest : MonoBehaviour
{
	private int _mass = 1;
	private decimal _pushCoef = 141m;

	void Update()
	{
	}

	void Start()
	{

	}

	void OnMouseDown()
	{
		Debug.Log("Clicked on IT! MouseDown");
		Calculate();
	}

	void ChangeHigh(float newHigh)
	{
		Vector3 newPos = new Vector3(transform.parent.position.x, newHigh, transform.parent.position.z);
		this.transform.parent.position = newPos;
	}

	void Calculate()
	{
		decimal dH1 = 0.002m * _pushCoef;
		float newHigh = this.transform.parent.position.y - (float)dH1;
		Debug.Log("dH1(Tetst) = " + dH1);
		Debug.Log("newHigh(Tetst) = " + newHigh);
		ChangeHigh(newHigh);

		GameObject Stick2 = GameObject.Find("Stick");
		decimal dH2 = 0.001m * _pushCoef;
		float newHigh2 = Stick2.transform.position.y + (float)dH2;

		Debug.Log("newHigh2(Tetst) = " + newHigh2);
		Vector3 newPos = new Vector3(Stick2.transform.position.x, newHigh2, Stick2.transform.position.z);
		Stick2.transform.position = newPos;
	}
}
