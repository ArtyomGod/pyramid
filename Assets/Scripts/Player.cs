﻿using System;
using System.Collections;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class Player : NetworkBehaviour
{
	[SyncVar]
	public int Id;

	[SyncVar]
	public int AdditionalMass = 1;

	[SyncVar]
	float m_seconds = 0;
	
	private float winTime = 0;

	StickInfosList _stickInfoList = new StickInfosList();
	
	[Command]
	public void CmdLog(int playersCount)
	{
		Debug.Log("Created at playercCount = " + playersCount);
	}

	[ClientRpc]
	public void RpcLog(int playersCount)
	{
		Debug.Log("Created at playercCount = " + playersCount);
	}

	public bool IsPlayerServer()
	{
		return isServer;
	}

	void Start()
	{
	}

	[ClientCallback]
	void Update()
	{
		if (!isLocalPlayer)
		{
			return;
		}
		if (!hasAuthority)
		{
			return;
		}
		if (isServer && NewGameController.Instance.m_players.Count == 2)
		{
			if (!NewGameController.Instance.IsWin)
			{
				m_seconds += Time.deltaTime;
			}
			RpcUpdateTime();
		}
		else if (!isServer)
		{
			m_seconds = NewGameController.Instance.m_timer.GetTime();
			CmdUpdateTime();
		}
		if (NewGameController.Instance.IsWin && winTime == 0)
		{
			winTime = (float)Math.Round(NewGameController.Instance.m_timer.GetTime(), 2);
			NewGameController.Instance.m_timer.SetPaused(true);
			RpcShowGameResultsText("Win!\n It took: " + winTime + " s");
		}

		/*if (NewGameController.Instance.MinusObject.GetComponent<Minus>().IsWin && winTime == 0)
		{
			winTime = (float)Math.Round(NewGameController.Instance.m_timer.GetTime(), 2);
			RpcShowGameResultsText("Win!\n It took: " + winTime + " s");
		}
		else
		{
			//MovePLayer();
		}*/
	}

	void FreezePozition()
	{
		this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX;
	}

	void UnfreezePozition()
	{
		this.GetComponent<Rigidbody>().constraints &= ~RigidbodyConstraints.FreezePositionX | ~RigidbodyConstraints.FreezePositionZ;
	}
	
	public IEnumerator WaitSetAnimationTrigger() {
		yield return new WaitForSeconds(1.5f);
		Debug.Log("Stop wait animation");
	}

	/*[Command]
	public void CmdChangeMaterialOnYellow()
	{
		this.GetComponentInChildren<MeshRenderer>().material = YellowLadyBug;
	}
	[ClientRpc]
	public void RpcChangeMaterialOnYellow()
	{
		this.GetComponentInChildren<MeshRenderer>().material = YellowLadyBug;
	}

	public void ChangeMaterialOnPurple()
	{
		this.GetComponentInChildren<MeshRenderer>().material = PurpleLadyBug;
	}*/

	[Command]
	public void CmdUpdateTime()
	{
			NewGameController.Instance.m_timer.SetTime(m_seconds);
	}

	[ClientRpc]
	public void RpcUpdateTime()
	{
			NewGameController.Instance.m_timer.SetTime(m_seconds);
	}

	[ClientRpc]
	public void RpcShowTime()
	{
		NewGameController.Instance.m_timePanel.SetActive(true);
		NewGameController.Instance.m_timer.SetPaused(false);
	}

	[Command]
	public void CmdShowTime()
	{
		NewGameController.Instance.m_timePanel.SetActive(true);
		NewGameController.Instance.m_timer.SetPaused(false);
	}

	[ClientRpc]
	public void RpcShowGameResultsText(string text)
	{
		//NewGameController.Instance.m_timePanel.SetActive(false);
		NewGameController.Instance.m_gameResultsPanel.GetComponentInChildren<Text>().text = text;
		NewGameController.Instance.m_gameResultsPanel.SetActive(true);
	}

	[Command]
	public void CmdShowGameResultsText(string text)
	{
		RpcShowGameResultsText(text);
	}

	[Command]
	public void CmdInitSticks()
	{
		NewGameController.Instance.SetGeneratedSticksInfo(_stickInfoList.ToArray());	
	}

	[ClientRpc]
	public void RpcInitSticks()
	{
		NewGameController.Instance.SetGeneratedSticksInfo(_stickInfoList.ToArray());
	}

	public void InitSticckInfos(StickInfo[] stickInfos)
	{
		foreach(var stickInfo in stickInfos)
		{
			_stickInfoList.Add(stickInfo);
		}
	}

	public void SetSticckInfos(StickInfo[] stickInfos)
	{
		int counter = 0;
		foreach (var stickInfo in stickInfos)
		{
			_stickInfoList[counter] = stickInfos[counter];
			counter++;
		}
	}


	[Command]
	public void CmdLogStickInfos()
	{
		Debug.Log(_stickInfoList.ToArray()[0]);
	}

	public void ChangeNewControllerStickInfos(int playerId, int mainStickIndex)
	{
		CmdChangeNewControllerStickInfos(playerId, mainStickIndex);	
	}

	//Изменяет StickInfos на Сервере
	[Command]
	public void CmdChangeNewControllerStickInfos(int playerId, int mainStickIndex)
	{
		NewGameController.Instance.UpdateSticks(playerId, mainStickIndex, AdditionalMass);
	}

	//Перераситывает Sticks на сервере
	[Command]
	public void CmdReculculateSticks() { }

	//Перераситывает Sticks на на Клиенте
	[ClientRpc]
	public void RpcReculculateSticks() { }

	[ClientRpc]
	public void RpcSetPlayingTrue()
	{
		NewGameController.Instance.IsPlaying = true;
	}

	[Command]
	public void CmdSetPlayingTrue()
	{
		NewGameController.Instance.IsPlaying = true;
	}

	[ClientRpc]
	public void RpcSetIsWinTrue()
	{
		NewGameController.Instance.IsWin = true;
	}

	[Command]
	public void CmdShowWinResult()
	{
		winTime = (float)Math.Round(NewGameController.Instance.m_timer.GetTime(), 2);
		NewGameController.Instance.ShowGameResult("Win!\n It took: " + winTime + " s");
	}

	[ClientRpc]
	public void RpcShowWinResult()
	{
		winTime = (float)Math.Round(NewGameController.Instance.m_timer.GetTime(), 2);
		NewGameController.Instance.ShowGameResult("Win!\n It took: " + winTime + " s");
	}

	[ClientRpc]
	public void RpcSetTagPlayer2()
	{
		this.tag = "Player2";
	}

	[Command]
	public void CmdSetWeight(int newMass)
	{
		AdditionalMass = newMass;
	}

	public void SetMass(int newMass)
	{
		Debug.Log("AAAAAAa");
		AdditionalMass = newMass;
		CmdSetWeight(newMass);
	}
}
