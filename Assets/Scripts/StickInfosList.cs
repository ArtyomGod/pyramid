﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class StickInfosList : SyncListStruct<StickInfo>
{
	public StickInfosList()
	{
	}

	public StickInfo[] ToArray()
	{
		StickInfo[] stickInfos = new StickInfo[16];
		int counter = 0;
		foreach (var stickInfo in this)
		{
			stickInfos[counter] = stickInfo;
			counter++;
		}

		return stickInfos;
	}
}
