﻿using Assets.Scripts;
using UnityEngine;

public class Stick : MonoBehaviour
{
	public GameObject ConnectedStick;
	public int SquareOfContainer;
	public int Mass = 1;
	private decimal _pushCoef = 141m;

	public float Level = 10f;
	public float UnRoundedLevel = 10;
	private float lengthOfStick = 0.664f;
	public bool IsMainStick = false;
	public bool IsBalanced = false;

	public Material Blue;
	public Material Green;
	public int Id;

	void Update()
	{
		if (ConnectedStick != null && NewGameController.Instance.IsPlaying)
		{
			CheckBalance();
		}
	}

	void Start ()
	{
		Mass = 1;
		Level = 10;
		UnRoundedLevel = Level;
		IsBalanced = false;
	}

	void OnMouseDown()
	{
		if (IsBalanced || !NewGameController.Instance.IsPlaying)
		{
			return;
		}
		
		NewGameController.Instance.InterractWithTouchedStick(this.transform.parent.gameObject);
	}

	public void ChangeHigh(float newHigh)
	{
		Vector3 newPos = new Vector3(transform.localPosition.x, newHigh, transform.localPosition.z);
		this.transform.localPosition = newPos;
	}

	public void CalculateHigh()
	{
		float newHigh = ((Level / 20f) * lengthOfStick) - 0.362f;
		ChangeHigh(newHigh);
	}

	public void CheckBalance()
	{
		if (IsBalanced)
		{
			return;
		}
		IsBalanced = Mathf.Abs(Level - ConnectedStick.GetComponent<Stick>().Level) <= 2 &&
		       Mathf.Abs(Level - ConnectedStick.GetComponent<Stick>().Level) >= 0;
		if (IsBalanced)
		{
			SetIndicatorGreen();
			SetLightHouseGreen();
		}
	}

	public void SetArrowBlueColor()
	{
		transform.GetChild(1).GetComponent<MeshRenderer>().material = Blue;
	}

	public void SetLightHouseGreen()
	{
		transform.GetChild(3).GetComponent<MeshRenderer>().material = Green;
	}

	public void SetIndicatorGreen()
	{
		transform.parent.GetChild(1).GetComponent<MeshRenderer>().material = Green;
	}
	
	public void Set(int mass, int square, int level, float unrounded)
	{
		Mass = mass;
		SquareOfContainer = square;
		Level = level;
		UnRoundedLevel = unrounded;
	}
}

